#include "cpu.h"
#include "ram.h"
#include <iostream>

void Compute(){
    int sum = 0;
    for(int i = 0; i < RAM_SIZE; i++){
        sum += ReadRAM(i);
    }
    std::cout << sum << std::endl;
}
