#include "ram.h"

int ram[RAM_SIZE];

void WriteRAM(int element, int data){
    if(element < RAM_SIZE)
        ram[element] = data;
}

int ReadRAM(int element){
    if(element < RAM_SIZE)
        return ram[element];
    else
        return 0;
}
