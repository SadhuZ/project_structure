#include "disk.h"
#include "ram.h"

#include <fstream>
#include <iostream>

void Save(){
    std::ofstream ram;
    ram.open("data.txt");
    if(!ram.is_open()){
        std::cout << "File data.txt open error." << std::endl;
        return;
    }
    for(int i = 0; i < RAM_SIZE; i++){
        ram << ReadRAM(i) << std::endl;;
    }
    ram.close();
}

void Load(){
    std::ifstream ram;
    ram.open("data.txt");
    if(!ram.is_open()){
        std::cout << "File data.txt open error." << std::endl;
        return;
    }
    for(int i = 0; i < RAM_SIZE; i++){
        int data;
        ram >> data;
        WriteRAM(i, data);
    }
    ram.close();
}
