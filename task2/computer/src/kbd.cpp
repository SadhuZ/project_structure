#include "kbd.h"
#include "ram.h"

#include <iostream>

void Input(){
    std::cout << "Enter ram data." << std::endl;
    for(int i = 0; i < RAM_SIZE; i++){
        int data;
        std::cout << "Addres " << i << ": ";
        std::cin >> data;
        WriteRAM(i, data);
    }
}
