#include <iostream>
#include "kbd.h"
#include "disk.h"
#include "gpu.h"
#include "cpu.h"

using namespace std;

int main()
{
    string command;
    while(1){
        cout << "> ";
        cin >> command;
        if(command == "sum"){
            Compute();
        }
        else if(command == "save"){
            Save();
        }
        else if(command == "load"){
            Load();
        }
        else if(command == "input"){
            Input();
        }
        else if(command == "display"){
            Display();
        }
        else if(command == "exit"){
            break;
        }
        else{
            cout << "Command not found." << endl;
        }
    }
    return 0;
}
