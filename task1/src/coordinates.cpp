#include "coordinates.h"
#include <iostream>

void PrintCoordinate(const Coordinate& point){
    std::cout << point.x << ", " << point.y << std::endl;
}

void ScanCoordinate(Coordinate& point){
    double x, y;
    std::cout << "Enter coordinate x: ";
    std::cin >> x;
    std::cout << "Enter coordinate y: ";
    std::cin >> y;
    point.x = x;
    point.y = y;
}

bool IsEqual(const Coordinate& point1, const Coordinate& point2){
    return (point1.x == point2.x) && (point1.y == point2.y);
}
