#include "surgery.h"
#include <iostream>

void Scalpel(Coordinate start, Coordinate end){
    std::cout << "Cut between (" << start.x << ", " << start.y
              << ") and (" << end.x << ", " << end.y << ")" << std::endl;
}

void Hemostat(Coordinate point){
    std::cout << "Hemostat on (" << point.x << ", " << point.y << ")" << std::endl;
}


void Tweezers(Coordinate point){
    std::cout << "Tweezers on (" << point.x << ", " << point.y << ")" << std::endl;
}


void Suture(Coordinate start, Coordinate end){
    std::cout << "Stitch between (" << start.x << ", " << start.y
              << ") and (" << end.x << ", " << end.y << ")" << std::endl;
}
