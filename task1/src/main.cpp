#include <iostream>
#include "coordinates.h"
#include "surgery.h"

using namespace std;

int main()
{
    Coordinate startPoint, endPoint;
    string command;

    while(1){
        cout << "Enter command: ";
        cin >> command;
        if(command == "scalpel"){
            cout << "Enter start point." << endl;
            ScanCoordinate(startPoint);
            cout << "Enter end point." << endl;
            ScanCoordinate(endPoint);
            Scalpel(startPoint, endPoint);
            break;
        }
        else{
            cout << "Wrong command!" << endl;
        }
    }

    while(1){
        cout << "Enter command: ";
        cin >> command;
        if(command == "hemostat"){
            Coordinate currentPoint;
            cout << "Enter a point." << endl;
            ScanCoordinate(currentPoint);
            Hemostat(currentPoint);
        }
        else if(command == "tweezers"){
            Coordinate currentPoint;
            cout << "Enter a point." << endl;
            ScanCoordinate(currentPoint);
            Tweezers(currentPoint);
        }
        else if(command == "suture"){
            Coordinate stitchStartPoint, stitchEndPoint;
            cout << "Enter start point." << endl;
            ScanCoordinate(stitchStartPoint);
            cout << "Enter end point." << endl;
            ScanCoordinate(stitchEndPoint);
            if(IsEqual(stitchStartPoint, startPoint) && IsEqual(stitchEndPoint, endPoint)){
                Suture(stitchStartPoint, stitchEndPoint);
                cout << "Operation is over." << endl;
                break;
            }
            else{
                cout << "Coordinates are not equal!" << endl;
            }
        }
        else{
            cout << "Wrong command!" << endl;
        }
    }

    return 0;
}
