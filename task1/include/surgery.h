#ifndef SURGERY_H
#define SURGERY_H

#include "coordinates.h"

/* scalpel — принимает на вход две двумерные координаты начала и конца разреза или линии отсечения.
 * При выполнении этой команды в консоль выводится сообщение о том,
 * что был сделан разрез между введёнными координатами.
 */
void Scalpel(Coordinate start, Coordinate end);

/* hemostat — эта команда принимает на вход одну точку,
 * в которой требуется сделать зажим, о чём также сообщает в консоль.
 */
void Hemostat(Coordinate point);

/* tweezers — пинцет, как и зажим, принимает одну точку для применения.
 * Сообщение об этом выводится в консоль.
 */
void Tweezers(Coordinate point);

/* suture — команда хирургической иглы, которая делает шов между двумя указанными точками.
 */
void Suture(Coordinate start, Coordinate end);

#endif // SURGERY_H
