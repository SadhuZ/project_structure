#ifndef COORDINATES_H
#define COORDINATES_H

struct Coordinate{
    double x;
    double y;
};

void PrintCoordinate(const Coordinate& point);

void ScanCoordinate(Coordinate& point);

bool IsEqual(const Coordinate& point1, const Coordinate& point2);

#endif // COORDINATES_H
